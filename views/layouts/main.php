<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <a href="<?= Yii::$app->homeUrl ?>">
        <h1 class="text-center text-light bg-dark">List of articles:</h1>
    </a>
    <div class="d-flex justify-content-between">
        <a class="btn btn-lg btn-success " href="<?= Yii::$app->homeUrl ?>">Home</a>
        <a class="btn btn-lg btn-success " href="<?= Url::to(['site/add-article'])?>">Add new article</a>
    </div>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="d-flex justify-content-between">
        <p class=" ml-5">&copy; Neodimius <?= date('Y') ?></p>

        <p class="mr-5"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
