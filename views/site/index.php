<?php

/* @var $this yii\web\View */

$this->title = 'List of Articles';

use yii\helpers\Url; ?>
<div class="site-index">
    <?php if (!empty($message)) :?>
        <div class="alert alert-<?= $message_type; ?>" role="alert">
            <?= $message; ?>
        </div>
    <?php endif; ?>

    <?php echo \yii\bootstrap4\LinkPager::widget([
        'pagination' => $pagination,
    ]);?>

    <div class="bg-dark">
        <span class="text-light">Sort by: </span>
        <!--    Sort by:-->
        <?php echo $sort->link('title') . ' | ' . $sort->link('time'); ?>
    </div>

    <div class="d-flex justify-content-between flex-wrap">
        <?php foreach ($articles as $article):?>
            <div class="card border-dark mb-3" style="width: 18rem;">
                <div class="card-header d-flex justify-content-between">
                    <a href="<?= Url::to(['site/edit-article']) . '&id=' . $article['id']; ?>" class="btn btn-warning">Edit</a>
                    <a href="<?= Url::to(['site/delete-article']) . '&id=' . $article['id']; ?>" class="btn btn-danger">Delete</a>
                </div>
                <div class="card-body text-dark">
                    <h5 class="card-title"><?= $article['title'] ?></h5>
                    <p class="card-text"><?= $article['short_description'] ?></p>
                    <a href="<?= Url::to(['site/article']) . '&id=' . $article['id']; ?>" class="btn btn-primary">Read more</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php echo \yii\bootstrap4\LinkPager::widget([
        'pagination' => $pagination,
    ]);?>
</div>
