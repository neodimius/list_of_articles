<?php
/* @var $this yii\web\View */

use mihaildev\ckeditor\CKEditor;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Add new article';
?>

<h1><?= $this->title ?></h1>

<?php $form = ActiveForm::begin([
    'method' => 'post',
    'options' => ['enctype' => 'multipart/form-data'],
    'action' => Url::to(['site/add-article']),
]) ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'short_description')->textarea(array('rows'=>3,'cols'=>5)) ?>
<?= $form->field($model, 'text')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'basic', //basic, standart or full
        'inline' => false,
        'height' => 500,
    ],]); ?>

<?= Html::submitButton('Add article', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>
