<?php
/* @var $this yii\web\View */

use yii\web\View;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;

$this->title = 'Edit article';
?>

<h1><?= $this->title ?></h1>

<?php $form = ActiveForm::begin([
    'method' => 'post',
    'options' => ['enctype' => 'multipart/form-data'],
    'action' => Url::to(['site/edit-article']),
]) ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'short_description')->textarea(array('rows'=>3,'cols'=>5)) ?>
<?= $form->field($model, 'text')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'basic', //basic, standart or full
        'inline' => false,
        'height' => 500,
    ],]); ?>
<?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

<?= Html::submitButton('Edit article', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end() ?>

