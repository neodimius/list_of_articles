<?php

/* @var $this yii\web\View */

$this->title = $article['title'];

use yii\helpers\Url;
use yii\web\View; ?>
<div class="site-index">
    <div class="jumbotron">
        <h1 class="display-4"><?= $this->title; ?></h1>
        <hr class="my-4">
        <a href="<?= Url::to(['site/edit-article']) . '&id=' . $article['id']; ?>" class="btn btn-warning">Edit</a>
        <a href="<?= Url::to(['site/delete-article']) . '&id=' . $article['id']; ?>" class="btn btn-danger">Delete</a>
    </div>
    <p class="text-justify"><?= $article['text']; ?></p>
</div>
