<?php

namespace app\controllers;

use app\models\AddArticleForm;
use app\models\Articles;
use Yii;
use yii\data\Pagination;
use yii\data\Sort;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //Get all Articles
        $query = Articles::find();

        // Config for sort widget
        $sort = new Sort([
            'attributes' => [
                'age',
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'title',
                ],
                'time' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label' => 'time',
                ],
            ],
        ]);

        // Config for Pagination
        $pagination = new Pagination([
        'defaultPageSize' => 6,
        'totalCount' => $query->count(),
        ]);

        //Getting Articles by params
        $articles = $query -> orderBy($sort->orders)
        -> offset($pagination->offset)
        -> limit ($pagination->limit)
        -> all();

        return $this->render('index', [
            'articles' => $articles,
            'pagination' => $pagination,
            'sort' => $sort,
            'message' => Yii::$app->request->get('message') ?: '',
            'message_type' => Yii::$app->request->get('message_type') ?: '',
        ]);
    }

    /**
     * Adding new Article
     * @return string|Response
     */
    public function actionAddArticle()
    {
        $form = new AddArticleForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate() ) {
            $article = new Articles();
            $article->title = $form->title;
            $article->short_description = $form->short_description;
            $article->text = $form->text;
            $article->save();

            return $this->redirect('index.php?message=adding the article successfully&message_type=success');

        } else {
            return $this->render('addArticle', ['model' => $form]);
        }
    }

    /**
     * Deleting Article by id
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteArticle()
    {
        $request = Yii::$app->request;
        if ($request->get('id')){
            $query = Articles::findOne($request->get('id'));
            $query->delete();
            $this->redirect('index.php?message=deleting the article successfully&message_type=warning');
        } else {
            $this->redirect('index.php?message=the article not found&message_type=danger');
        }
    }

    /**
     * Editing Article
     * @return string|Response
     */
    public function actionEditArticle()
    {
        $form = new AddArticleForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate() ) {
            $article = Articles::findOne($form->id);
            $article->title = $form->title;
            $article->short_description = $form->short_description;
            $article->text = $form->text;
            $article->save();

            return $this->redirect('index.php?message=editing the article successfully&message_type=success');

        } elseif ($id = Yii::$app->request->get('id')) {
            $article = Articles::findOne(Yii::$app->request->get('id'));
            if (!$article) {
                $this->redirect('index.php?message=the article not found&message_type=danger');
            } else {
                $form->title = $article['title'];
                $form->short_description = $article['short_description'];
                $form->text = $article['text'];
                $form->id = $article['id'];

                return $this->render('editArticle', ['model' => $form]);
            }
        } else {
            $this->redirect('index.php?message=the article not found&message_type=danger');
        }
    }

    /**
     * Opening Article by id
     * @return string
     */
    public function actionArticle()
    {
        $request = Yii::$app->request;
        if ($request->get('id')){
            $article = Articles::findOne($request->get('id'));

            if ($article) {
                return $this->render('article', [
                    'article' => $article,
                ]);
            } else {
                $this->redirect('index.php?message=the article not found&message_type=danger');
            }
        } else {
            $this->redirect('index.php?message=the article not found&message_type=danger');
        }
    }
}
