<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AddArticleForm extends \yii\base\Model {

    public $title;
    public $short_description;
    public $text;
    public $id;

    public function rules() {

        return [

            [['title', 'short_description', 'text'], 'required'],
            ['title', 'string', 'min' => 2, 'max' => 25],
            ['short_description', 'string', 'min' => 5, 'max' => 250],
            ['text', 'string', 'min' => 50],
            ['id', 'number'],
        ];

    }

}
